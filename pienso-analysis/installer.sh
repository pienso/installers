#!/usr/bin/env bash

echo "[START] PIENSO ANALYSIS DIRECTORY SETUP"

USERNAME=$(whoami)
sudo mkdir -p /opt/ubq/analysis &&
sudo chmod g+s /opt/ubq/analysis  &&
sudo mkdir -p /opt/ubq/analysis/db/volume &&
sudo mkdir -p /opt/ubq/analysis/backend/volume &&
sudo chown -R $USERNAME /opt/ubq

echo "[END] PIENSO ANALYSIS DIRECTORY SETUP"
