# Pienso Analysis API Server  Setup Instructions:

Please make sure you have _root_ access before continuing. 

To bootstrap & subsequently install the Pienso demo you need 2 files:

* `installer.sh`
* `production.yml`

You will be provided with dockerhub credentials that are controlled by the Pienso domain. Please use these credentials further down the document. 

### OS X Users
Before you begin, please download the docker-ce installer [here](https://store.docker.com/editions/community/docker-ce-desktop-mac)

### Centos Users

Before you begin, please download the docker-ce installer  [here] (https://docs.docker.com/install/linux/docker-ce/centos/)

You must also install docker-compose [here] (https://docs.docker.com/compose/install/#install-compose)

## Installing
Please clone the _installer_ project to your users home folder. You will need your root credentials

```
$ ./pienso-analysis/installer.sh
```

This will create a series of folders in `/opt/ubq/analysis`. This is where all your application data will be stored. 

#### OS X Users

You will need to share your /opt/ubq folders. Click on the docker whale -> preferences -> sharing -> Add */opt/ubq* - 

You will need to bump dockers RAM to at least 8GB.  Click on the docker whale -> preferences -> advanced -> slide to 8GB if RAM

Apply and restart the docker daemon. 

## Login to Pienso on DockerHub
You will need to login with the docker hub user credentials that where provided to you upon signup.

```
docker login -u YOUR_DOCKER_HUB_USERNAME -p YOUR_DOCKER_HUB_PASSWORD
```

### Point to your Pienso Lensing platform

Pienso Analysis API server will pull finished models from the lensing platform. You will need to provide the hostname of the box where you are hosting the lensing platform 

Create a `docker.env` file in your `pienso-analysis` directory. This will be used to configure networking for your analysis server.

Your docker.env file needs to look similar to the following:

```
PIENSO_LENS_API_URL=<your.lensing.com>
PIENSO_LENS_API_PORT=80
```

You can replace the url, with the domain or IP that you are using to serve the lensing application.  Note that the URL should not include `http://`

### Compose File
In order to launch Pienso, you will need a docker compose file. This is also located in your extracted files.

To start Pienso please execute the following from within `pienso-analysis`:

```
docker-compose -f production.yml up -d
```

You should now have Pienso Analysis API running at `http://<your box's hostname>`

## Docker 101

To update Pienso Analysis API Server, you can update the project. 

~~~
docker-compose -f production.yml pull
~~~

To stop Pienso:

~~~
docker-compose -f production.yml stop
~~~


### Trouble Shooting

Problem: If you see an error similar to the following:

```
---
ERROR: repository pienso/po-redis not found: does not exist or no pull access
---
```
Solution: This means you are not logged into docker hub. You can re-login at anytime using `docker login`.
