# Release Notes

## Release: 1.0.1

### Core:
	pienso/po-api 		0.0.0.100 		sha256:91d2a0352031c833605cd0070271e710892dc9b033dd630cb8f60d91059a3797
        pienso/po-web 		0.0.0.220 		sha256:b91a5874f7f44a90ee4e4821a3cddc5eace2edd80fc4797d7ec6291a3b3fc4ae
	pienso/po-ml 		0.0.0.70 		sha256:c29a0ed45233bbbdb15e2266dbeea719c954f5dfce620fb291baf086e16f3aca
	pienso/po-minio 	0.0.0.5 		sha256:560408bf9d6dcff60e602b0d90c85013d268f769010e698ec354f49cc79d79f7
	pienso/po-redis 	0.0.0.70		sha256:12acb9265382f8867bf1cb6cf3570af0a26228f315553a3603d63f5482a11610
	pienso/po-postgres 	0.0.0.100 		sha256:f3178b9ccc3d032f829af0328aea3cc5432ae8fc14b3b1e4ea77ef8d57e9765c

### Analysis:
	pienso/as-backend 	0.0.0.99 		sha256:99bc1c8be75d7028b170b00def99c9cadc7edc078b9eb6b914f962cca2b9ed14
	pienso/as-postgres 	0.0.0.94 		sha256:82849172660c9265ba90246a1b481cdaea49ceb9fca74ba5cccd9c78137f2ee6
        pienso/as-web 		0.0.0.99 		sha256:7ce2deca11aea5cc33cd9fa9485106e9733fae0780d2b767bbe0d779c3c41314
