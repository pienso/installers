#!/usr/bin/env bash

echo "[START] PIENSO DIRECTORY SETUP"

USERNAME=$(whoami)
 sudo mkdir -p /opt/ubq/pienso/ &&
 sudo chmod g+s /opt/ubq/pienso/  &&
 sudo mkdir -p /opt/ubq/pienso/db/volume  &&
 sudo mkdir -p /opt/ubq/pienso/redis/volume  &&
 sudo mkdir -p /opt/ubq/pienso/api/volume/  &&
 sudo mkdir -p /opt/ubq/pienso/web/volume/  &&
 sudo mkdir -p /opt/ubq/pienso/minio/volume/  &&
 sudo chown -R $USERNAME /opt/ubq

echo "[END] PIENSO DIRECTORY SETUP"
