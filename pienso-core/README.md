# Pienso Lensing Platform Setup Instructions:

Please make sure you have _root_ access before continuing. 

To bootstrap & subsequently install the Pienso demo you need 2 files:

* `installer.sh`
* `production.yml`

You will be provided with dockerhub credentials that are controlled by the Pienso domain. Please use these credentials further down the document. 

### OS X Users
Before you begin, please download the docker-ce installer [here](https://store.docker.com/editions/community/docker-ce-desktop-mac)

### Centos Users

Before you begin, please download the docker-ce installer  [here] (https://docs.docker.com/install/linux/docker-ce/centos/)

You must also install docker-compose [here] (https://docs.docker.com/compose/install/#install-compose)

## Installing
Please clone the _installer_ project to your users home folder. You will need your root credentials

```
$ ./pienso-core/installer.sh
```

This will create a series of folders in `/opt/ubq/pienso`. This is where all your application data will be stored.

#### OS X Users

You will need to share your /opt/ubq folders. Click on the docker whale -> preferences -> sharing -> Add */opt/ubq* - 

You will need to bump dockers RAM to at least 8GB.  Click on the docker whale -> preferences -> advanced -> slide to 8GB if RAM

Apply and restart the docker daemon. 

## Login to Pienso on DockerHub
You will need to login with the docker hub user credentials that where provided to you upon signup.

```
docker login -u YOUR_DOCKER_HUB_USERNAME -p YOUR_DOCKER_HUB_PASSWORD
```

### Pienso License

You will need to add the Pienso License to your production.yml file.  This will be provided to you in your welcome email.

You will need to add the following to po-api: 

```
PIENSO_LICENSE=XXXXXXXXX
```

### Compose File
In order to launch Pienso, you will need a docker compose file. This is also located in your extracted files.

To start Pienso please execute the following:

```
docker-compose -f production.yml up -d
```

You should now have Pienso running at `http://localhost`

## Performance Tuning

Pienso defaults to using 3 cores. This will work well for a traditional macbook pro. If you are on a xeon multi-core cpu, please add the following ENV Config to your production.yml file. It should be the number of cores - 1. In our example, we're targetting a 14 core xeon. 

`PIENSO_ML_CORES=13`

##### Example

```
  po-ml:
    image:
      pienso/po-ml
    restart: unless-stopped
    environment:
      - PIENSO_REDIS_URL=po-redis
      - PIENSO_API_URL=po-api
      - PIENSO_ML_CORES=13
    scale: 3
    networks:
      - "po-net"
```

## Docker 101

To update pienso, you can update the project. 

~~~
docker-compose -f production.yml pull
~~~

To stop Pienso:

~~~
docker-compose -f production.yml stop
~~~


### Trouble Shooting

Problem: If you see an error similar to the following:

```
---
ERROR: repository pienso/po-redis not found: does not exist or no pull access
---
```
Solution: This means you are not logged into docker hub. You can re-login at anytime using `docker login`.
